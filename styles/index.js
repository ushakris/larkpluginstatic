import "./cube-portfolio/cubeportfolio.css";

import "./footers/footer-v2.css";

import "./headers/header-default-centered.css";
import "./headers/header-default.css";
import "./headers/header-v1.css";
import "./headers/header-v2.css";
import "./headers/header-v3.css";
import "./headers/header-v4-centered.css";
import "./headers/header-v4.css";
import "./headers/header-v5.css";
import "./headers/header-v6.css";
import "./headers/header-v7.css";
import "./headers/header-v8.css";
import "./plugins/style-switcher.css";

import "./animate.css";
import "./app.css";
import "./blocks.css";
import "./custom-cubeportfolio.css";
import "./custom.css";
import "./dark-red.css";
import "./gallery.css";
import "./ie8.css";
import "./image.css";
import "./layout.less";
import "./modal.css";
import "./plugins.css";
import "./style.css";
import "./typeahead.css";

//images folder
//import "./img";